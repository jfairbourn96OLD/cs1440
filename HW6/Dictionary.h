//
// Created by Justin Fairbourn on 4/5/2017.
//

#ifndef HW6_DICTIONARY_H
#define HW6_DICTIONARY_H

#include "KeyValue.h"
#include <string>
#include <iostream>

template <class K, class V>
class Dictionary {
public:
    Dictionary<K, V>() {
        m_allocated = 10;
        m_size = 0;
        m_dictionary = new KeyValue<K, V> *[m_allocated];
        m_sortedArray = new KeyValue<K, V> *[m_allocated];
    }

    Dictionary<K, V>(int size) {
        m_allocated = size;
        m_size = 0;
        m_dictionary = new KeyValue<K, V> *[m_allocated];
        m_sortedArray = new KeyValue<K, V> *[m_allocated];
    }

    Dictionary<K, V>(const Dictionary<K, V> &copy) {
        m_allocated = copy.getAllocated();
        m_size = copy.getSize();
        m_dictionary = new KeyValue<K, V> *[m_allocated];
        m_sortedArray = new KeyValue<K, V> *[m_allocated];
        for (int i = 0; i < m_size; i++) m_sortedArray[i] = new KeyValue<K, V>(copy.getBySortedIndex(i));
        for (int i = 0; i < m_size; i++) m_dictionary[i] = new KeyValue<K, V>(copy.getByIndex(i));
    }

    void add(KeyValue<K, V> *newEntry) {
        if (findInDictionary(newEntry->getKey()) >= 0) throw "KEY ALREADY IN DICTIONARY";
        if (m_size == m_allocated) {
            grow(m_dictionary);
            grow(m_sortedArray);
        }
        if (m_size == 0) {
            m_sortedArray[0] = newEntry;
            m_dictionary[0] = newEntry;
        }else {
            m_dictionary[m_size] = newEntry;
            sortedAdd(newEntry, 0, m_size - 1);
        }
        m_size++;
    }

    KeyValue<K, V> *getByKey(K key) const {
        int found = findInDictionary(key);
        if (found >= 0) return m_sortedArray[found];
        else throw "KEY NOT FOUND";
    };

    KeyValue<K, V> *getByIndex(int i) const {
        if (i < 0 || i >= m_size) throw "INDEX OUT OF RANGE";
        else return m_dictionary[i];
    }

    int getSize() const { return m_size; }

    int getAllocated() const { return m_allocated; }

    void removeByKey(K key) {
        int found = findInDictionary(key);
        if (found == -1) throw "KEY NOT FOUND";
        else removeByIndex(found);
        //removeByIndex(found);
    }

    void removeByIndex(int i) {
        if (i < 0 || i >= m_size) throw "INDEX OUT OF RANGE";
        delete m_dictionary[i];
        for (int j = i; j < m_size - 1; j++) {
            m_dictionary[j] = m_dictionary[j + 1];
        }
        m_dictionary[m_size - 1] = nullptr;
        m_size--;
    }

private:
    KeyValue<K, V> **m_dictionary = nullptr;
    int m_allocated = 0;
    int m_size = 0;

    //By changing the grow() method to accept a pointer to a dynamic array, we can tell it to grow
    //both arrays by only chaning the method minimally.
    void grow(KeyValue<K, V> **oldDictionary) {
        m_allocated = m_allocated * 2;
        KeyValue<K, V> **newDictionary = new KeyValue<K, V> *[m_allocated];
        for (int i = 0; i < m_size; i++) newDictionary[i] = oldDictionary[i];
        delete[] oldDictionary;
        oldDictionary = newDictionary;
    }

    int findInDictionary(K key) const {
        return sortedFind(key, 0, m_size-1);
        /** Old crappy code
        for (int i = 0; i < m_size; i++) {
            if (m_dictionary[i]->getKey() == key) return i;
        }
        return -1;
        */
     }
/* By assuming the data type 'K' can use the '<' operator, we can implement a sorting algorithm.
 * Once the data is sorted in a separate dynamic array, we can retrieve KeyValues by searching with a
 * binary sort, giving us a complexity of O(log(n)) rather than O(n) as before.
 */
private:
    /**This array will use the same m_size and m_allocated variables as the unsorted data.*/
    KeyValue<K, V> **m_sortedArray;

    /**
     * This method is called by the original add method and adds the KeyValue pair to the sortedArray where it belongs.
     * The algorithm is simple, but far from ideal, and uses a binary search to recursively find the appropriate index
     * for the new KeyValue, and then adds it.
     * @param newPair The new KeyValue<K,V> pair being added (assumed sortable)
     * @min The minimum index passed to this call of the function.
     * @max The maximum index passed to this call of the function
     */
    void sortedAdd(KeyValue<K, V> *newPair, int min, int max) {
        K key = newPair->getKey();
        int mid = (min + max) / 2;
        if (key < m_sortedArray[mid]->getKey() && key > m_sortedArray[mid-1]->getKey()) {
            //we've found the correct index, add it
            for (int i = m_size; i > mid; i--) {
                m_sortedArray[i] = m_sortedArray[i - 1];
            }
            m_sortedArray[mid] = newPair;
            return;
        } else {
            if (newPair->getKey() < m_sortedArray[mid]->getKey()) {
                max = mid;
            } else {
                min = mid + 1;
            }
            sortedAdd(newPair, min, max);
        }
    }

    /**
     * This Method searches through the sortedArray using a binary search to determine if the key is in the dictionary.
     * @param pair The KeyValue pair with the key being searched for.
     * @param min The minimum index passed to this call of the function.
     * @param max The maximum index passed to this call of the function.
     * @return the int value of the index if the Key is in the dictionary. -1 if it's not there.
     */
    int sortedFind(K key, int min, int max) const
    {
        if(min > max){
            return -1;
        }
        int mid = (min + max) / 2;
        if(m_sortedArray[min]->getKey() == key) {
            return mid;
        }
        else if(key < m_sortedArray[mid]->getKey()){
            return sortedFind(key, min, max);
        }else{
            return sortedFind(key, mid+1, max);
        }
    }

    KeyValue<K,V> *getBySortedIndex(int i ) const {return m_sortedArray[i];}
};


#endif //HW6_DICTIONARY_H
